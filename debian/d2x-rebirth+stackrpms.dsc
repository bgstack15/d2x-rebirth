Format: 3.0 (quilt)
Source: d2x-rebirth
Binary: d2x-rebirth
Architecture: any
Version: 0.60.0-1+stackrpms
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://www.dxx-rebirth.com/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 9~), imagemagick, libphysfs-dev, libsdl1.2-dev, libsdl-image1.2-dev, libsdl-mixer1.2-dev, scons
XS-Autobuild: yes
Package-List:
 d2x-rebirth deb non-free/games optional arch=any
Files:
 00000000000000000000000000000000 1 d2x-rebirth.orig.tar.gz
 00000000000000000000000000000000 1 d2x-rebirth+stackrpms.debian.tar.xz
